<!doctype html>
<html>
    <head>
        <title>
            Acces BDD avec PDO
        </title>
    </head>
    
    <body>
        <?php
        
        try
        {
            //definition de constantes
            
            
            define('SRV','localhost');
            define('BD', 'bibliotheque');
            define('USER', 'root');
            define('PW', '');     
            
            $cnx = new PDO('mysql:host='.SRV.'; dbname='.BD, USER, PW, 
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8", PDO::ATTR_ERRMODE =>PDO::ERRMODE_EXCEPTION));
            
            //création requête
            
            $sql ="SELECT*FROM auteur";
            
            // execution requête
            
            $idRequete = $cnx -> query($sql);
            
            // Lecture
            
            while($now = $idRequete -> fetch(PDO::FETCH_ASSOC))
            {
                echo $now['id_auteur'].' / '.$now['nom'].' '.$now['prenom'].'<br>';
            }                     
            echo '<br>';
            $cnx = null;
        }             
        catch (PDOException $montre)
        {
             echo "Erreur : ".$montre->getMessage();
             exit;
        }
        
    
        ?>
    </body>    
    
</html>

